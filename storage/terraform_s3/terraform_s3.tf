provider "aws" {
  region = "eu-central-1"
}

resource "aws_s3_bucket" "b" {
  bucket = "larry-brand.terraform"
  acl    = "private"

  tags = {
    Name        = "Terraform"
  }
}
