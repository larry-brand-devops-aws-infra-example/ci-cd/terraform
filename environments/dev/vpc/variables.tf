variable "environment" {
  type = string
}

variable "vpc_name" {
  description = "VPC name"
  type = string
}

variable "vpc_tags_Name" {
  description = "VPC tags:Name"
  type = string
}

variable "private_subnets_tags_Name" {
  description = "Subnets tags:Name"
  type = string
}

variable "public_subnets_tags_Name" {
  description = "Subnets tags:Name"
  type = string
}

variable "region" {
  description = "AWS region"
  type = string
}

