terraform {
  backend "s3" {
    bucket = "larry-brand.terraform"
    key    = "environments/dev/vpc"
    region = "eu-central-1"
  }
}
