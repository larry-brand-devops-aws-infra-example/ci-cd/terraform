provider "aws" {
  region  = var.region
}

data "aws_eks_cluster" "cluster" {
  name = module.eks.cluster_id
}

data "aws_availability_zones" "available" {
}

module "eks" {
  source  = "terraform-aws-modules/eks/aws"
  version = "16.0.1"

  cluster_name                    = var.cluster_name
  cluster_version                 = "1.20"
  subnets                         = data.aws_subnet_ids.selected.ids
  cluster_create_timeout          = "1h"
  cluster_endpoint_private_access = true

  vpc_id =  data.aws_vpc.selected.id

  worker_groups = [
    {
      name                          = "worker-group-1"
      instance_type                 = var.instance_type
      additional_userdata           = "echo foo bar"
      asg_desired_capacity          = 1
      //asg_min_size                = 2
      //asg_max_size                = 3
      additional_security_group_ids = [aws_security_group.worker_group_mgmt_one.id]
    },

  ]

  worker_additional_security_group_ids = [aws_security_group.all_worker_mgmt.id]
  map_roles                            = var.map_roles
  map_users                            = var.map_users
  map_accounts                         = var.map_accounts
}

provider "kubernetes" {
  host                   = data.aws_eks_cluster.cluster.endpoint
  cluster_ca_certificate = base64decode(data.aws_eks_cluster.cluster.certificate_authority.0.data)
  token                  = data.aws_eks_cluster_auth.cluster.token
  load_config_file       = false
}
