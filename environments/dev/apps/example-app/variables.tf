variable "environment" {
  type = string
}

variable "cluster_name" {
  type = string
}

variable "instance_count" {
  type = string
}

variable "instance_type" {
  type = string
}

variable "region" {
  description = "AWS region"
  type = string
}

### VPC by tags

data "aws_vpc" "selected" {
  filter {
    name = "tag:Name"
    values = [var.vpc_tags_Name]
  }
}

variable "vpc_tags_Name" {
  type = string
}

### Subnets by tags

data "aws_subnet_ids" "selected" {
  tags = {
    Name = var.subnets_tags_Name
  }
  vpc_id =  data.aws_vpc.selected.id
}

variable "subnets_tags_Name" {
  type = string
}

### Etc

variable "map_accounts" {
  description = "Additional AWS account numbers to add to the aws-auth configmap."
  type        = list(string)

  default = [
    "777777777777",
    "888888888888",
  ]
}

variable "map_roles" {
  description = "Additional IAM roles to add to the aws-auth configmap."
  type = list(object({
    rolearn  = string
    username = string
    groups   = list(string)
  }))

  default = [
    {
      rolearn  = "arn:aws:iam::66666666666:role/role1"
      username = "role1"
      groups   = ["system:masters"]
    },
  ]
}

variable "map_users" {
  description = "Additional IAM users to add to the aws-auth configmap."
  type = list(object({
    userarn  = string
    username = string
    groups   = list(string)
  }))

  default = [
    {
      userarn  = "arn:aws:iam::66666666666:user/user1"
      username = "user1"
      groups   = ["system:masters"]
    },
    {
      userarn  = "arn:aws:iam::66666666666:user/user2"
      username = "user2"
      groups   = ["system:masters"]
    },
  ]
}

