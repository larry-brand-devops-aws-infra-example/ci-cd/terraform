terraform {
  backend "s3" {
    bucket = "larry-brand.terraform"
    key    = "environments/dev/apps/example-app"
    region = "eu-central-1"
  }
}
